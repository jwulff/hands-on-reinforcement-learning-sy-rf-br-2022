U
    �� b�D  �                   @   s`   d Z ddlmZ ddlmZmZmZ ddlZddl	Z	ddl
mZ ddlmZ G dd	� d	e�ZdS )
z�
**Module gathering all general input parameters used for the simulation.**
    :Authors: **Alexandre Lasheen**, **Danilo Quartullo**, **Helga Timko**
�    )�division)�str�range�objectN)�c�   )�RingOptionsc                   @   sR   e Zd ZdZdddddde� fdd�Zdd� Zd	d
� Zdd� Zdd� Z	dd� Z
dS )�Ringa+   Class containing the general properties of the synchrotron that are
    independent of the RF system or the beam.

    The index :math:`n` denotes time steps, :math:`k` ring segments/sections
    and :math:`i` momentum compaction orders.

    Parameters
    ----------
    ring_length : float (opt: float array [n_sections])
        Length [m] of the n_sections ring segments of the synchrotron.
        An RF station, a synchrotron radiation kick, and/or an impedance kick
        can be included at the end of each ring section.
    alpha_0 : float (opt: float array/matrix [n_sections, n_turns+1])
        Momentum compaction factor of zeroth order :math:`\alpha_{0,k,i}` [1];
        can be input as single float or as a program of (n_turns + 1) turns
        (should be of the same size as synchronous_data).
        In case of higher order momentum compaction, check the
        documentation for the inputs: alpha_order, alpha_1, alpha_2
    synchronous_data : float (opt: float array/matrix [n_sections, n_turns+1])
        Design synchronous particle momentum (default) [eV], kinetic or
        total energy [eV] or bending field [T] on the design orbit.
        Input for each RF section :math:`p_{s,k,n}`.
        Can be input as a single constant float, or as a
        program of (n_turns + 1) turns. In case of several sections without
        acceleration, input: [[momentum_section_1], [momentum_section_2],
        etc.]. In case of several sections with acceleration, input:
        [momentum_program_section_1, momentum_program_section_2, etc.]. Can
        be input also as a tuple of time and momentum, see also
        'cycle_time' and 'PreprocessRamp'
    Particle : class
        A Particle-based class defining the primary, synchronous particle (mass
        and charge) that is reference for the momentum/energy in the ring.
    n_turns : int
        Optional: Number of turns :math:`n` [1] to be simulated.
        If a synchronous_data program is passed as a tuple (see below),
        the number of turns will be overwritten depending on the length in time
        of the program
    synchronous_data_type : str
        Optional: Choice of 'synchronous_data' type; can be 'momentum'
        (default), 'total energy', 'kinetic energy' or 'bending field'
        (requires bending_radius to be defined)
    bending_radius : float
        Optional: Radius [m] of the bending magnets,
        required if 'bending field' is set for the synchronous_data_type
    n_sections : int
        Optional: number of ring sections/segments; default is 1
    alpha_1 : float (opt: float array/matrix [n_sections, n_turns+1])
        Momentum compaction factor of first order
        :math:`\alpha_{1,k,i}` [1]; can be input as single float or as a
        program of (n_turns + 1) turns (should be of the same size as
        synchronous_data and alpha_0).
    alpha_2 : float (opt: float array/matrix [n_sections, n_turns+1])
        Optional : Momentum compaction factor of second order
        :math:`\alpha_{2,k,i}` [1]; can be input as single float or as a
        program of (n_turns + 1) turns (should be of the same size as
        synchronous_data and alpha_0).
    RingOptions : class
        Optional : A RingOptions-based class with default options to check the
        input and initialize the momentum program for the simulation.
        This object defines the interpolation scheme, plotting options, etc.
        The options for this object can be adjusted and passed to the Ring
        object.

    Attributes
    ----------
    ring_circumference : float
        Circumference of the synchrotron. Sum of ring segment lengths,
        :math:`C = \sum_k L_k` [m]
    ring_radius : float
        Radius of the synchrotron, :math:`R = C/(2 \pi)` [m]
    bending_radius : float
        Bending radius in dipole magnets, :math:`\rho` [m]
    alpha_order : int
        Highest order of momentum compaction (as defined by the input). Can
        be 0,1,2.
    alpha_0 : float matrix [n_sections, n_turns+1]
        Zeroth order momentum compaction factor
        :math:`\alpha_{0,k,n}`
    alpha_1 : float matrix [n_sections, n_turns+1]
        First order momentum compaction factor
        :math:`\alpha_{1,k,n}`
    alpha_2 : float matrix [n_sections, n_turns+1]
        Second order momentum compaction factor
        :math:`\alpha_{2,k,n}`
    eta_0 : float matrix [n_sections, n_turns+1]
        Zeroth order slippage factor :math:`\eta_{0,k,n} = \alpha_{0,k,n} -
        \frac{1}{\gamma_{s,k,n}^2}` [1]
    eta_1 : float matrix [n_sections, n_turns+1]
        First order slippage factor :math:`\eta_{1,k,n} =
        \frac{3\beta_{s,k,n}^2}{2\gamma_{s,k,n}^2} + \alpha_{1,k,n} -
        \alpha_{0,k,n}\eta_{0,k,n}` [1]
    eta_2 : float matrix [n_sections, n_turns+1]
        Second order slippage factor :math:`\eta_{2,k,n} =
        -\frac{\beta_{s,k,n}^2\left(5\beta_{s,k,n}^2-1\right)}
        {2\gamma_{s,k,n}^2} + \alpha_{2,k,n} - 2\alpha_{0,k,n}\alpha_{1,k,n}
        + \frac{\alpha_{1,k,n}}{\gamma_{s,k,n}^2} + \alpha_{0,k}^2\eta_{0,k,n}
        - \frac{3\beta_{s,k,n}^2\alpha_{0,k,n}}{2\gamma_{s,k,n}^2}` [1]
    momentum : float matrix [n_sections, n_turns+1]
        Synchronous relativistic momentum on the design orbit :math:`p_{s,k,n}`
    beta : float matrix [n_sections, n_turns+1]
        Synchronous relativistic beta program for each segment of the
        ring :math:`\beta_{s,k}^n = \frac{1}{\sqrt{1
        + \left(\frac{m}{p_{s,k,n}}\right)^2} }` [1]
    gamma : float matrix [n_sections, n_turns+1]
        Synchronous relativistic gamma program for each segment of the ring
        :math:`\gamma_{s,k,n} = \sqrt{ 1
        + \left(\frac{p_{s,k,n}}{m}\right)^2 }` [1]
    energy : float matrix [n_sections, n_turns+1]
        Synchronous total energy program for each segment of the ring
        :math:`E_{s,k,n} = \sqrt{ p_{s,k,n}^2 + m^2 }` [eV]
    kin_energy : float matrix [n_sections, n_turns+1]
        Synchronous kinetic energy program for each segment of the ring
        :math:`E_{s,kin} = \sqrt{ p_{s,k,n}^2 + m^2 } - m` [eV]
    delta_E : float matrix [n_sections, n_turns+1]
        Derivative of synchronous total energy w.r.t. time, for all sections,
        :math:`: \quad E_{s,k,n+1}- E_{s,k,n}` [eV]
    t_rev : float array [n_turns+1]
        Revolution period turn by turn.
        :math:`T_{0,n} = \frac{C}{\beta_{s,n} c}` [s]
    f_rev : float array [n_turns+1]
        Revolution frequency :math:`f_{0,n} = \frac{1}{T_{0,n}}` [Hz]
    omega_rev : float array [n_turns+1]
        Revolution angular frequency :math:`\omega_{0,n} = 2\pi f_{0,n}` [1/s]
    cycle_time : float array [n_turns+1]
        Cumulative cycle time, turn by turn, :math:`t_n = \sum_n T_{0,n}` [s].
        Possibility to extract cycle parameters at these moments using
        'parameters_at_time'.
    RingOptions : RingOptions()
        The RingOptions is kept as an attribute of the Ring object for further
        usage.

    Examples
    --------
    >>> # To declare a single-section synchrotron at constant energy:
    >>> # Particle type Proton
    >>> from beam.beam import Proton
    >>> from input_parameters.ring import Ring
    >>>
    >>> n_turns = 10
    >>> C = 26659
    >>> alpha_0 = 3.21e-4
    >>> momentum = 450e9
    >>> ring = Ring(C, alpha_0, momentum, Proton(), n_turns)
    >>>
    >>>
    >>> # To declare a two section synchrotron at constant energy and
    >>> # higher-order momentum compaction factors; particle Electron:
    >>> from beam.beam import Electron
    >>> from input_parameters.ring import Ring
    >>>
    >>> n_turns = 10
    >>> C = [13000, 13659]
    >>> alpha_0 = [[3.21e-4], [2.89e-4]]  # or [3.21e-4, 2.89e-4]
    >>> alpha_1 = [[2.e-5], [1.e-5]]  # or [2.e-5, 1.e-5]
    >>> alpha_2 = [[5.e-7], [5.e-7]]  # or [5.e-7, 5.e-7]
    >>> momentum = 450e9
    >>> ring = Ring(C, alpha_0, momentum, Electron(), n_turns,
    >>>             alpha_1=alpha_1, alpha_2=alpha_2)

    �   �momentumNc                 C   s~  t |�| _t |�| _tj|dtd�| _t�| j�| _| jdtj	  | _
|d k	rZt|�| _n|| _| jt| j�krxtd��|| _|| _|j|| j| jd|| jj| jj| j| jd�	| _| jjd | jd kr�| jjd d | _t�d� t�dd| jj| j d   �| _t�d| j| jj d  �| _t�| jd | jjd  �| _t�| jd | jjd  �| jj | _tj| jdd�| _t�| jd| jt  �| _ t�!| j �| _"d| j  | _#dtj	 | j# | _$|j%d k�r�| j"}n| j"|j% }|j|| j| j|d	�| _&d
| _'|	d k	�r(|j|	| j| j|d	�| _(d| _'nt�)| j&j�| _(|
d k	�rb|j|
| j| j|d	�| _*d| _'nt�)| j&j�| _*| �+�  d S )Nr
   )ZndminZdtyper   zDERROR in Ring: Number of sections and ring length size do not match!T)Zinput_to_momentum�synchronous_data_type�mass�chargeZcircumference�bending_radiuszbWARNING in Ring: The number of turns for the simulation was changed by passing a momentum program.)Zaxis)�interp_timer   ),�int�n_turns�
n_sections�npZarray�float�ring_length�sumZring_circumferenceZpiZring_radiusr   �len�RuntimeError�Particler   Zreshape_datar   r   r   �shape�warnings�warnZsqrt�beta�gamma�energy�
kin_energy�diff�delta_E�dotr   �t_revZcumsum�
cycle_time�f_rev�	omega_revZt_start�alpha_0�alpha_order�alpha_1�zeros�alpha_2�eta_generation)�selfr   r)   Zsynchronous_datar   r   r   r   r   r+   r-   r   r   � r0   ��C:\Users\jwulff\workspaces\optimizing-rf-manipulations-in-the-ps-using-reinforcement-learning\BLonD\blond\input_parameters\ring.py�__init__�   s�    

�
"�  �
  �
  �zRing.__init__c              	   C   sd   t | jd �D ]}t| dt|� ��  qt | jd d�D ]&}t| d| t�| j| jd g�� q8dS )a[   Function to generate the slippage factors (zeroth, first, and
        second orders, see [1]_) from the momentum compaction and the
        relativistic beta and gamma program through the cycle.

        References
        ----------
        .. [1] "Accelerator Physics," S. Y. Lee, World Scientific,
                Third Edition, 2012.
        r
   Z_eta�   zeta_%sN)	r   r*   �getattrr   �setattrr   r,   r   r   �r/   �ir0   r0   r1   r.     s    �zRing.eta_generationc                 C   sL   t �| j| jd g�| _td| j�D ]"}| j| | j| d  | j|< q$dS )z> Function to calculate the zeroth order slippage factor eta_0 r
   r   g       �N)r   �emptyr   r   �eta_0r   r)   r   r6   r0   r0   r1   �_eta04  s    z
Ring._eta0c                 C   sv   t �| j| jd g�| _td| j�D ]L}d| j| d  d| j| d   | j|  | j	| | j
|   | j|< q$dS )z= Function to calculate the first order slippage factor eta_1 r
   r   r3   r   N)r   r8   r   r   Zeta_1r   r   r   r+   r)   r9   r6   r0   r0   r1   �_eta1;  s    "��z
Ring._eta1c                 C   s�   t �| j| jd g�| _td| j�D ]�}| j| d  d| j| d  d  d| j| d   | j|  d| j	|  | j
|   | j
| | j| d   | j	| d | j|   d| j| d  | j	|  d| j| d    | j|< q$dS )z> Function to calculate the second order slippage factor eta_2 r
   r   r   �   r3   N)r   r8   r   r   Zeta_2r   r   r   r-   r)   r+   r9   r6   r0   r0   r1   �_eta2C  s*    $���������z
Ring._eta2c                 C   s  i }t �|| j| jd �|d< t �|| j| jd �|d< t �|| j| jd �|d< t �|| j| jd �|d< t �|| j| jd �|d< t �|| j| j�|d< t �|| j| j	�|d< t �|| j| j
�|d	< t �|| j| jd �|d
< t �|| jdd� t �| jd ��|d< |S )aL   Function to return various cycle parameters at a specific moment in
        time. The cycle time is defined to start at zero in turn zero.

        Parameters
        ----------
        cycle_moments : float array
            Moments of time at which cycle parameters are to be calculated [s].

        Returns
        -------
        parameters : dictionary
            Contains 'momentum', 'beta', 'gamma', 'energy', 'kin_energy',
            'f_rev', 't_rev'. 'omega_rev', 'eta_0', and 'delta_E' interpolated
            to the moments contained in the 'cycle_moments' array

        r   r   r   r   r    r!   r'   r%   r(   r9   r
   Nr#   )r   Zinterpr&   r   r   r   r    r!   r'   r%   r(   r9   r"   )r/   Zcycle_momentsZ
parametersr0   r0   r1   �parameters_at_timeN  sB    
�
�
�
�
�
�
�
�
��zRing.parameters_at_time)�__name__�
__module__�__qualname__�__doc__r   r2   r.   r:   r;   r=   r>   r0   r0   r0   r1   r	      s    #    �
er	   )rB   Z
__future__r   �builtinsr   r   r   Znumpyr   r   Zscipy.constantsr   Zinput_parameters.ring_optionsr   r	   r0   r0   r0   r1   �<module>
   s   